angular.module('starter.controllers', [])

.controller('AppCtrl', ['$scope', '$timeout', 'AdMobService',
 function($scope, $timeout, AdMobService) {
    $scope.showContent = false;
    $scope.showMenu = false;
    $timeout(function() { $scope.showContent = true; } , 300);
    $timeout(function() { $scope.showMenu = true; } , 600);
    $timeout(function() { AdMobService.showAd(); } , 1000);
}])

.controller('RegisterController', ['$scope', 'Credentials', 'Register', '$state', '$ionicHistory', '$timeout', '$cordovaDialogs', 'MyKeyboard', 'ConnectionTestFactory',
  function($scope, Credentials, Register, $state, $ionicHistory, $timeout, $cordovaDialogs, MyKeyboard, ConnectionTestFactory) {  
  $scope.page = { 'state' : 'waiting' , 'title' : "", 'isValid' : true , 'name' : '' , 'email' : '', 'telephone' : ''};
  var credentials = Credentials.get();

  $scope.registerSuccess = function() {
    $ionicHistory.clearCache();
    $ionicHistory.nextViewOptions({
      disableAnimate: true,
      disableBack: true
    });
    $timeout(function() { $state.go('app.priceinformationlistquery'); } , 100);
  };

  $scope.doRegister = function() {
    $scope.page.title = "";
    var device = "not_supported";
    if(ionic && ionic.Platform && ionic.Platform.platform()) {
      device = ionic.Platform.platform();
    }
    var version = "not_supported";
    if(ionic && ionic.Platform && ionic.Platform.version()) {
      version = ionic.Platform.version();
    }

    var register =new Register();
    register.device = device;
    register.namesurname = $scope.page.name;
    register.namesurname = $scope.page.name;
    register.email = $scope.page.email;
    register.telephone = $scope.page.telephone;
    register.version = version;
    $scope.page.state = 'saving';
    register.$save( function(registeredUser) {
      credentials.username = registeredUser.username;
      credentials.password = registeredUser.password;
      Credentials.set(credentials);
      $scope.registerSuccess();      
    }, function(error) {
      var message = "Sunucuya bağlanamıyor. Lütfen internet bağlantınızı kontrol edin." + error;
      $cordovaDialogs.alert( message, "Bağlantı Kurulamadı", "Yeniden dene").then(function() {
            $timeout(function() { $state.go('app.priceinformationlistquery'); } , 100);
        });
    });
  };

  $scope.continuePressed = function() {
    $scope.page.isValid = $scope.page.name.length > 0;
    if($scope.page.isValid) {
      $scope.doRegister();
    }
  };

  $scope.resetValidation =function () {
    $scope.page.isValid = true;
  };

  $scope.serverError = function() {
    $cordovaDialogs.alert("Sunucu bağlantısı kurulamadı. Lütfen İnternet bağlantınızı kontrol ederek tekrar deneyiniz.", "Bağlantı", "Tamam")
        .then(function() {
            $ionicHistory.goBack(true);
        }); 
  };

  ConnectionTestFactory.get( function(success) {
    if(success[1] == 'O' && success[2] == 'K') {
      if(credentials.username !== undefined) {
        $scope.registerSuccess();      
      } else {
        $scope.page.state = 'register';
        $scope.page.title = "Hoşgeldiniz..";
        MyKeyboard.show('namesurname', 250);  
      }
    } else {
      $scope.serverError();
    }    
  }, function(error) {
     $scope.serverError();
  });

}])

.controller('FeedController', ['$scope', 'PriceInformationFeed', '$cordovaSocialSharing', 'PriceInformation', '$cordovaDialogs',
  function($scope, PriceInformationFeed, $cordovaSocialSharing, PriceInformation, $cordovaDialogs) {    
    $scope.feed = { 'count':25 , 'offset':0 , 'isLoadMoreActive':false};
    $scope.items = [];
    $scope.doRefresh = function() {
        $scope.feed.offset = 0;
        $scope.items = [];
        $scope.$broadcast('scroll.refreshComplete');
        $scope.loadMore();        
    };
    $scope.loadMore = function() {
        if($scope.feed.offset > 0) {
          $scope.feed.offset += $scope.feed.count;
        }
        
        if(!$scope.feed.isLoadMoreActive) {
          $scope.feed.isLoadMoreActive = true;
          
          var newItems = PriceInformationFeed.query(
            { 'count': $scope.feed.count , 'offset': $scope.feed.offset },
            function (success) { 
              $scope.items = $scope.items.concat(newItems);
              $scope.feed.isLoadMoreActive = false;
              $scope.$broadcast('scroll.infiniteScrollComplete'); 
            });

        }
    };
    $scope.date = function(datetext) { return new Date(datetext); };
    $scope.number = function(numbertext) {return parseFloat(numbertext); };

    $scope.fix = function(item) {
      var message = "Olması gereken fiyatı giriniz.";
      $cordovaDialogs.prompt( message  , item.productName,  ['İptal','Düzelt'], '').then( 
        function(result) {
          var value = Number(result.input1);        
          if(result.buttonIndex == 2 && value !== null && value > 0 ) {
              var priceInformation = new PriceInformation();
              priceInformation.date = item.date;
              priceInformation.tradeCenterId = item.tradeCenterId;
              priceInformation.productId = item.productId;
              priceInformation.price = value;
              priceInformation.$save( function(successResult) {        
                  $cordovaDialogs.alert("Teşekkür ederiz.", "İşlem Başarılı", "Tamam");
              });
          }          
      });
    };    
    $scope.approve = function(item) {
      var message = item.productName + " fiyatı " + item.price + " TL bildirilmiş. Bu fiyata katılıyor musunuz?";
      $cordovaDialogs.confirm( message  , 'Onay',  ['İptal','Onayla']).then( 
        function(buttonIndex) {        
          if(buttonIndex == 2 ) {
              var priceInformation = new PriceInformation();
              priceInformation.date = item.date;
              priceInformation.tradeCenterId = item.tradeCenterId;
              priceInformation.productId = item.productId;
              priceInformation.price = Number(item.price);
              priceInformation.$save( function(successResult) {        
                  $cordovaDialogs.alert("Teşekkür ederiz.", "İşlem Başarılı", "Tamam");
              });
          }          
      });
    };
    $scope.share = function(item) {
      var message = item.date + " tarihi " + item.tradeCenterName + " için " + item.productName + " fiyatı " + item.price + " TL";
      var subject = "Enn Piyasa";
      var file = item.imageUrl;
      var link = "http://www.enntaze.com/market";
      $cordovaSocialSharing.share(message, subject, file, link);
    };


    

    /**************************************
    $scope.initAd = function() {
        if ( window.plugins && window.plugins.AdMob ) {
          var ad_units = {
        ios : {
          banner: 'ca-app-pub-4344154765553377/8516110246',
          interstitial: 'ca-app-pub-4344154765553377/8516110246'
        },
        android : {
          banner: 'ca-app-pub-4344154765553377/8516110246',
          interstitial: 'ca-app-pub-4344154765553377/8516110246'
        },
        wp8 : {
          banner: 'ca-app-pub-4344154765553377/8516110246',
          interstitial: 'ca-app-pub-4344154765553377/8516110246'
        }
          };
          var admobid = "";
          if( /(android)/i.test(navigator.userAgent) ) {
            admobid = ad_units.android;
          } else if(/(iphone|ipad)/i.test(navigator.userAgent)) {
            admobid = ad_units.ios;
          } else {
            admobid = ad_units.wp8;
          }
            window.plugins.AdMob.setOptions( {
                publisherId: admobid.banner,
                interstitialAdId: admobid.interstitial,
                bannerAtTop: false, // set to true, to put banner at top
                overlap: false, // set to true, to allow banner overlap webview
                offsetTopBar: false, // set to true to avoid ios7 status bar overlap
                isTesting: false, // receiving test ad
                autoShow: true // auto show interstitial ad when loaded
            });
            $scope.registerAdEvents();
            alert('admob ready!');
        } else {
            alert( 'admob plugin not ready' );
        }
    };
  
    $scope.registerAdEvents = function() {
      document.addEventListener('onReceiveAd', function(){});
        document.addEventListener('onFailedToReceiveAd', function(data){});
        document.addEventListener('onPresentAd', function(){});
        document.addEventListener('onDismissAd', function(){ });
        document.addEventListener('onLeaveToAd', function(){ });
        document.addEventListener('onReceiveInterstitialAd', function(){ });
        document.addEventListener('onPresentInterstitialAd', function(){ });
        document.addEventListener('onDismissInterstitialAd', function(){ });
    };

    if(window.plugins && window.plugins.AdMob) {
      $scope.initAd();
      window.plugins.AdMob.createBannerView();
    }
    */
}])

.controller('QueryController', ['$scope', 'DateService', 'ParameterService', 'Product', 'TradeCenter',
 function($scope, DateService, ParameterService, Product, TradeCenter){
  
  $scope.queryParams = {
      "date": "",
      "tradeCenter": "",
      "product": ""
  };

  $scope.dates = DateService.datesAroundToday();
  $scope.initDateSelect = function() {
     var index = Math.floor($scope.dates.length / 2);
     $scope.queryParams.date = $scope.dates[index];
  };

  $scope.tradeCenters = TradeCenter.query();
  $scope.products = Product.query();
  
  $scope.isQueryValid = 1;
  $scope.formValid = function() {
    $scope.isQueryValid = 1;
  };
  
  $scope.query = function(queryParams) {
    $scope.hrefResultList = "#";
    ParameterService.setParameter(queryParams);
    $scope.isQueryValid = 
    (queryParams.tradeCenter !== "" && queryParams.tradeCenter !== null) || 
    (queryParams.product !== "" && queryParams.product !== null);
    if($scope.isQueryValid)
    {
      ParameterService.setParameter(queryParams);
      $scope.hrefResultList = "#/app/market/resultList";
    }
  };
}])

.controller('ResultListController', ['$scope', 'ParameterService', 'PriceInformationlist', 'DateService', '$ionicHistory', '$cordovaDialogs', '$state', '$filter',
  function($scope, ParameterService, PriceInformationlist, DateService, $ionicHistory, $cordovaDialogs, $state, $filter ){
  $scope.queryParams = ParameterService.getParameter();
  $scope.shouldShowDelete =0;
  $scope.shouldShowReorder =0;
  $scope.listCanSwipe=1;

  $scope.informationText = "-";
  var tradeCenterId = 0;
  var productId = 0;

  if($scope.queryParams.tradeCenter !== "" && $scope.queryParams.tradeCenter !== null ) {
    $scope.informationText = $scope.queryParams.tradeCenter.name;
    tradeCenterId = $scope.queryParams.tradeCenter.id;
  }
  if($scope.queryParams.product !== "" && $scope.queryParams.product !== null ) {
    $scope.informationText = $scope.queryParams.product.name;
    productId = $scope.queryParams.product.id;
  }
  
  $scope.items = [];
  var date = DateService.convertDateToYYYYMMDD($scope.queryParams.date);
  $scope.allItems = PriceInformationlist.query({date:date, tradecenterId:tradeCenterId, productId:productId }, function() {
      var processedIds = [];
      angular.forEach( $scope.allItems, function(item, order) {
        var index = processedIds.indexOf( item.id );
        if( index == -1) {
          item.totalPoints = item.points;
          item.totalCount = item.count;
          item.reliability = 100;
          item.href = '#';
          $scope.items.push(item);
          processedIds.push(item.id);
        }
        else
        {
          var existingItem = $scope.items[index];
          existingItem.href =  "#app/market/resultList/" + existingItem.id;
          existingItem.totalPoints = Number(existingItem.totalPoints) + Number(item.points);
          existingItem.totalCount = Number(existingItem.totalCount) + Number(item.count);          
          existingItem.reliability =  existingItem.points / existingItem.totalPoints * 100;
          if( existingItem.reliability > 80 && existingItem.totalPoints > 100 ) {
            existingItem.href = '#';
          }
        }

        $scope.queryParams.items = $scope.allItems;
        ParameterService.setParameter($scope.queryParams);
      });

      if($scope.allItems.length === 0) {
        /*
        var message = "Aradığınız kriterlerde fiyat bulunamadı.";
        $cordovaDialogs.confirm( message  , 'Bilgilendirme',  ['İptal', 'Önceki Gün']).then( 
          function(buttonIndex) {        
            if(buttonIndex == 1 ) {
              $ionicHistory.goBack(true);
            } else if(buttonIndex == 2 ) {
              
                var params = ParameterService.getParameter();
                var date = new Date(DateService.convertDateToYYYYMMDD(params.date));
                date.setDate(date.getDate()-1);                
                params.date = $filter('date')(date, "dd.MM.yyyy");
                ParameterService.setParameter(params);
                $state.go($state.current, {}, {reload: true});              
            }          
        });
        */

        $cordovaDialogs.alert("Aradığınız kriterlerde fiyat bulunamadı.", "Bilgilendirme", "Tamam").then(function() { 
          $ionicHistory.goBack(true);
        }); 

      }
  });
}])

.controller('ResultOneProductController', ['$scope', '$stateParams', 'ParameterService', 'PriceInformation', 'DateService', '$ionicHistory', '$cordovaDialogs',
  function ($scope, $stateParams, ParameterService, PriceInformation, DateService, $ionicHistory, $cordovaDialogs) {
    
    $scope.queryParams = ParameterService.getParameter();
    $scope.items = $scope.queryParams.items;
    $scope.productInformationId = $stateParams.productInformationId;
    
    $scope.selectedItem = {};
    $scope.title = "";
    $scope.totalCount = 0;
    $scope.totalPoints = 0;
    $scope.imagePath = "#";
    $scope.productItems = [];
    $scope.alternateOption = {};

    angular.forEach($scope.items, function(item, order) {
      if(item.id == $scope.productInformationId) {
        $scope.imagePath = item.img;
        $scope.title = item.productName;
        $scope.totalCount = Number(item.count) + Number($scope.totalCount);
        $scope.totalPoints = Number(item.points) + Number($scope.totalPoints);

        if(item.reliability > 0)
          $scope.selectedItem = item;
        else
          $scope.productItems.push(item);
      }
    });

    $scope.expectSecondTouch = function(item) {
      var isFirstTouch = (item.price != $scope.alternateOption.price);
      if (isFirstTouch) {
        $scope.alternateOption = item;
      } else {
        $scope.alternateOption = {};
      }
    };

    $scope.sendPriceInformationToServer = function() {
      var item = $scope.alternateOption;
      if(item.id > 0) {
        var priceInformation = new PriceInformation();
        priceInformation.date = DateService.convertDateToYYYYMMDD($scope.queryParams.date);
        priceInformation.tradeCenterId = item.trade_center_id;
        priceInformation.productId = item.product_id;
        priceInformation.price = Number(item.price);
        priceInformation.$save( function(successResult) {
          $scope.alternateOption = {};
          $cordovaDialogs.alert("Katıldığınız için teşekkür ederiz.", "İşlem Başarılı", "Tamam")
          .then(function() {
              $ionicHistory.clearCache();
              $ionicHistory.goBack(true);
          }); 
        });
      }
    };

    $scope.name = "okan test " + $scope.productInformationId;
}])

.controller('InputDateAndTradeCenterController', ['$scope', 'DateService', 'ParameterService', 'DataInputService', 'TradeCenter',
  function($scope, DateService, ParameterService, DataInputService, TradeCenter ){
  $scope.queryParams = { "date": "", "tradeCenter": "" };
  $scope.isQueryValid = 1;

  $scope.dates = DateService.datesAroundToday();
  $scope.tradeCenters = TradeCenter.query();
  
  $scope.initDateSelect = function() {
     var index = Math.floor($scope.dates.length / 2);
     $scope.queryParams.date = $scope.dates[index];
  };
    
  $scope.formValid = function() {
    $scope.isQueryValid = 1;
  };

  $scope.inputData = function(queryParams) {
    $scope.hrefResultList = "#";
    ParameterService.setParameter(queryParams);
    $scope.isQueryValid = queryParams.tradeCenter !== '';
    if($scope.isQueryValid)
    {
      DataInputService.reset();
      ParameterService.setParameter(queryParams);
      $scope.hrefResultList = "#/app/dataInput/priceInformation";
    }
  };
}])

.controller('InputPriceInformationController', ['$scope', 'ParameterService', 'Product', 'PriceInformation', '$filter', 'DataInputService', '$timeout', '$ionicScrollDelegate', 'DateService' , 'MyKeyboard', '$ionicHistory', '$cordovaDialogs', '$state',
  function($scope, ParameterService, Product, PriceInformation, $filter, DataInputService, $timeout, $ionicScrollDelegate, DateService, MyKeyboard, $ionicHistory, $cordovaDialogs, $state ){
  
  $scope.queryParams = ParameterService.getParameter();
  $scope.selectedTradeCenter = $scope.queryParams.tradeCenter;
  $scope.priceInformationList = DataInputService.getPriceInformationList();
  allProducts = Product.query( function(success) {
    $scope.queryParams.allProducts = allProducts;
    ParameterService.setParameter($scope.queryParams);
  });


  $scope.isAvaibleToSend = function() {
    return DataInputService.getPriceInformationList().length > 0;
  };

  $scope.sendData = function() {
    var itemsToSend = $scope.priceInformationList.length;
    $scope.priceInformationList.forEach(function(item) {
      var priceInformation = new PriceInformation();
      priceInformation.date = DateService.convertDateToYYYYMMDD($scope.queryParams.date);
      priceInformation.tradeCenterId = $scope.selectedTradeCenter.id;
      priceInformation.productId = item.product.id;
      priceInformation.price = Number(item.price);
      priceInformation.$save( function(successResult) {        
        if(--itemsToSend === 0)
        {
          $cordovaDialogs.alert("Katıldığınız için teşekkür ederiz.", "İşlem Başarılı", "Tamam")
          .then(function() {
              $ionicHistory.goBack(true);
          });
        }
      }, function(error) {});
    });
  };

  $scope.showDeleteFor = function(priceInformation) {    
      if(priceInformation.showDelete) {
        priceInformation.showDelete = !priceInformation.showDelete;
      } else {
        $scope.priceInformationList.forEach(function(item) {
          item.showDelete = false;
        });
        $timeout(function() {priceInformation.showDelete = true;} , 10);
      } 
  };

  $scope.remove = function(priceInformation) {
    DataInputService.remove(priceInformation);
  };

  $scope.showAddProduct = function()  { $state.go('app.dataInputPriceInformationDetail'); };
}])

.controller('InputPriceInformationDetailController', ['$scope', 'ParameterService', 'PriceInformation', '$filter', 'DataInputService', '$timeout', '$ionicScrollDelegate', 'DateService' , 'MyKeyboard', '$ionicHistory', '$cordovaDialogs', '$state',
  function($scope, ParameterService, PriceInformation, $filter, DataInputService, $timeout, $ionicScrollDelegate, DateService, MyKeyboard, $ionicHistory, $cordovaDialogs, $state ) {
  
  $scope.parameters = ParameterService.getParameter();
  $scope.priceInformationEntity = { };
  $scope.products = [];
  $scope.allProducts = $scope.parameters.allProducts;

  $timeout(function() { $scope.products = angular.copy($scope.allProducts); } , 500);
    

  $scope.trimAdedProducts = function() {
    $scope.products = angular.copy($scope.allProducts);
    var definedProducts = DataInputService.getPriceInformationList();
    for(var i=0; i<definedProducts.length; i++) {
      var product = definedProducts[i].product;
      var productToRemove = $filter('filter')($scope.products, {id:product.id})[0];
      var index  = $scope.products.indexOf(productToRemove);
      $scope.products.splice(index, 1);
    }
  };

  $scope.resetFormValidation = function () {
    $scope.priceInformationEntity.isValid = true;
  };

  $scope.isModalValid = function() {
    return (typeof $scope.priceInformationEntity.product !== 'undefined') &&
       $scope.priceInformationEntity.price > 0;
  };

  $scope.savePriceInformationEntity = function() {    
    if($scope.isModalValid()) {
      $scope.priceInformationEntity.isValid = true;
      DataInputService.setPrice($scope.priceInformationEntity.price);
      $ionicHistory.goBack(true);
    }
    else {
      $scope.priceInformationEntity.isValid = false;
    }
  };

  $scope.showSearchDiv = true;
  
  //Filter 
  $scope.showProductSelection = false;
  $scope.searchTextChanged = function() {
    $scope.showProductSelection = $scope.priceInformationEntity.searchText.length > 0;
    $scope.resetFormValidation();
  };

  $scope.reEnableSearch = function() {
    
    $scope.showSearchDiv = true;
    $scope.showProductSelection = true;
    $scope.priceInformationEntity.product = null;
    $scope.priceInformationEntity.isValid = true;
    MyKeyboard.show('productName', 250);
  };

  //TODO: Below piece can be directive!
  $scope.prePrice = 0;
  $scope.priceChanged = function() {
    $scope.priceInformationEntity.price = $scope.priceInformationEntity.price.replace(/[^0-9]/g,".");
    var currencyRegex = /^\d+(\.\d{1,2})?$/;
    var isValid = currencyRegex.test($scope.priceInformationEntity.price);
    
    if($scope.priceInformationEntity.price.lastIndexOf('.') != $scope.priceInformationEntity.price.indexOf('.'))
      $scope.priceInformationEntity.price = $scope.prePrice;    
    if($scope.priceInformationEntity.price == '.')
      $scope.priceInformationEntity.price = '0.';
    if($scope.priceInformationEntity.price.length === 0 ||
        isValid || 
        ($scope.priceInformationEntity.price.slice(-1) === '.' && $scope.priceInformationEntity.price.slice(-2) !== '..')
      )
      $scope.prePrice = $scope.priceInformationEntity.price;
    else
      $scope.priceInformationEntity.price = $scope.prePrice;
    $scope.resetFormValidation();
  };

  $scope.saveProduct = function(product) {    
    DataInputService.setProduct(product);
    $scope.priceInformationEntity.searchText = product.name;
    $scope.showProductSelection = false;
    $scope.showSearchDiv = false;    
    $scope.priceInformationEntity.product = product;    
    MyKeyboard.show('productPrice', 100);
  };

  $scope.cleanData = function() {
    $scope.trimAdedProducts();
    $scope.showSearchDiv = true;
    $scope.priceInformationEntity = { 'price':'', 'isValid':true};
  };
  $scope.cleanData();
  MyKeyboard.show('productName', 250);
  
}])

.controller('ProductListController', ['$scope', 'Product', 'MyKeyboard', '$filter', '$state', 'ParameterService',
  function($scope, Product, MyKeyboard, $filter, $state, ParameterService) {        
    $scope.items = [];
    $scope.resetForm = function() { 
      var items = Product.query( 
        function() {
          $scope.items = items;
          ParameterService.setParameter($scope.items);
          $scope.$broadcast('scroll.refreshComplete');
      }); 
    };
    $scope.resetForm();
    $scope.showAddProduct = function() { $state.go('app.addNewProduct'); };
}])

.controller('AddNewProductController', ['$scope', 'MyKeyboard', 'Product', '$filter', 'ParameterService', '$cordovaDialogs', '$ionicHistory',
  function($scope, MyKeyboard, Product, $filter, ParameterService, $cordovaDialogs, $ionicHistory) {
    $scope.newProduct = {};
    $scope.errMessage = null;
    $scope.items = ParameterService.getParameter();

    $scope.resetForm = function() { $scope.newProduct = {}; $scope.errMessage = null; ParameterService.setParameter(null); };
    $scope.showAddProduct = function() { MyKeyboard.show('productName', 250); };
    $scope.closeAddProduct = function() { 
      MyKeyboard.hide();
      $cordovaDialogs.alert("Yeni ürün başarı ile eklendi.", "İşlem Başarılı", "Tamam")
          .then(function() {
              $ionicHistory.clearCache();
              $ionicHistory.goBack(true);
          }); 
    };
    $scope.searchTextChanged = function() { $scope.errMessage = null; };
    $scope.saveNewProduct = function() { 
      var found = $filter('filter')($scope.items, {name: $scope.newProduct.name}, true);
      if (!$scope.newProduct.name || $scope.newProduct.name.length === 0) {
        $scope.errMessage = "Lütfen ürün adını eksiksiz sağlayınız.";
      } else if(found.length) {
        $scope.errMessage = "Bu ürün sistemde mevcuttur.";
      } else {
        var newProduct = new Product();
        newProduct.name = $scope.newProduct.name;
        newProduct.wikititle = $scope.newProduct.name;
        newProduct.image = "http://www.enntaze.com/img/fide.png";
        newProduct.$save( 
          function(success) { 
            $scope.resetForm();
            $scope.closeAddProduct();
          }
        );
      }      
    };
}])

.controller('TradeCenterListController', ['$scope', 'TradeCenter', 'MyKeyboard', '$filter', '$state', 'ParameterService',
  function($scope, TradeCenter, MyKeyboard, $filter, $state, ParameterService) {        
    $scope.items = [];
    $scope.resetForm = function() { 
      var items = TradeCenter.query(
        function() {
          $scope.items = items;
          ParameterService.setParameter($scope.items);
          $scope.$broadcast('scroll.refreshComplete');
        }); 
    };
    $scope.resetForm();
    $scope.showAddTradeCenter = function()  { $state.go('app.addNewTradeCenter'); };
}])

.controller('AddNewTradeCenterController', ['$scope', 'MyKeyboard', 'TradeCenter', '$filter', 'ParameterService', '$cordovaDialogs', '$ionicHistory',
  function($scope, MyKeyboard, TradeCenter, $filter, ParameterService, $cordovaDialogs, $ionicHistory) {
    $scope.newTradeCenter = {};
    $scope.errMessage = null;
    $scope.items = ParameterService.getParameter();
     
    $scope.resetForm = function() { $scope.newTradeCenter = {}; $scope.errMessage = null; ParameterService.setParameter(null); };    
    $scope.showAddTradeCenter = function()  { MyKeyboard.show('tradeCenterName', 250); };
    $scope.closeAddTradeCenter = function() { 
      MyKeyboard.hide();
      $cordovaDialogs.alert("Yeni piyasa başarı ile eklendi.", "İşlem Başarılı", "Tamam")
          .then(function() {
              $ionicHistory.clearCache();
              $ionicHistory.goBack(true);
          }); 
    };
    $scope.searchTextChanged = function() { $scope.errMessage = null; };
    $scope.saveNewTradeCenter = function() { 
      var found = $filter('filter')($scope.items, {name: $scope.newTradeCenter.name}, true);
      if (!$scope.newTradeCenter.name || $scope.newTradeCenter.name.length === 0) {
        $scope.errMessage = "Lütfen piyasa adını eksiksiz sağlayınız.";
      } else if(found.length) {
        $scope.errMessage = "Bu piyasa sistemde mevcuttur.";
      } else {
        var newTradeCenter = new TradeCenter();
        newTradeCenter.name = $scope.newTradeCenter.name;
        newTradeCenter.wikititle = $scope.newTradeCenter.name;
        newTradeCenter.$save( 
          function(success) { 
            $scope.resetForm();
            $scope.closeAddTradeCenter();
          }
        );        
      }      
    };   
}])

.controller('ProfileController', ['$scope', 'Product', 'TradeCenter', 'Credentials', 'ProfileInfo', '$cordovaDialogs', '$ionicHistory', '$timeout', '$state',
 function($scope, Product, TradeCenter, Credentials, ProfileInfo, $cordovaDialogs, $ionicHistory, $timeout, $state) {
  $scope.productCount = 0;
  $scope.tradeCenterCount = 0;
  $scope.profile = { "name":"" , "email":"", "telephone":"", "dirty":false};   

  var products = Product.query( function() {
    $scope.productCount = products.length;
  });
  var tradeCenters = TradeCenter.query(function() {
    $scope.tradeCenterCount = tradeCenters.length;  
  });

  var credentials = Credentials.get();
  $scope.username = credentials.username;
  $scope.password = credentials.password;
  $scope.servicePath = servicePath;

  $scope.checkIfDirty = function() {
    $scope.profile.dirty =
    $scope.cleanProfile && 
      ($scope.cleanProfile.name != $scope.profile.name || 
       $scope.cleanProfile.email != $scope.profile.email ||
       $scope.cleanProfile.telephone != $scope.profile.telephone
      );
  };

  var profileInfo = ProfileInfo.get(function() {
    $scope.profile = profileInfo;
    $scope.profile.ready = true;
    $scope.cleanProfile = angular.copy($scope.profile);
  });
  
  $scope.save = function() {
    var profileInfo = new ProfileInfo();
    profileInfo.name = $scope.profile.name;
    profileInfo.email = $scope.profile.email;
    profileInfo.telephone = $scope.profile.telephone;
    profileInfo.$save( function(success) {      
      $cordovaDialogs.alert("Bilgileriniz başarı ile güncellendi", success.message, "Tamam");
      $scope.profile.dirty = false;  
    });
  };

  $scope.resetUser = function() {
    var message = "Kayıtlı tüm kullanıcı bilgileriniz sıfılanacaktır. Emin misiniz?";
    $cordovaDialogs.confirm( message  , 'Onay',  ['İptal','Sıfıla']).then( 
        function(buttonIndex) {        
          if(buttonIndex == 2 ) {
              window.localStorage.clear();
              $ionicHistory.clearCache();
              $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true
              });
              $timeout(function() { $state.go('app.register'); } , 100);
              
          }          
      });
  };

}])

.controller('WebInfoController', ['$scope', '$stateParams', '$sce',
  function ($scope, $stateParams, $sce) {
  $scope.title = $stateParams.title;
  //$scope.wikiAddress = $sce.trustAsResourceUrl( 'http://tr.m.wikipedia.org/wiki/' + $scope.title + '#content');
  $scope.wikiAddress = $sce.trustAsResourceUrl( 'http://tr.m.wikipedia.org/wiki/' + $scope.title + '?content=true');

}])

//Add new Controllers here...
;