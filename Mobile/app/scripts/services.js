angular.module('starter.services', [])

.service('ParameterService', function(){
	var parameter;
	this.setParameter = function(parameter) {
		this.parameter = parameter;
	};
	this.getParameter = function() {
		if(!this.parameter)
			this.parameter = {};
		return this.parameter;
	};
})

.service('DateService', function() {
	this.datesAroundToday = function() {
		var now = new Date();
            now.setHours(0);
            now.setMinutes(0);
            now.setSeconds(0);
            now.setMilliseconds(0);
            now.setDate(now.getDate() - 6);
            var dates = [];
            for (var i = 0; i < 11; i++) {
                var dateValue = now.setDate(now.getDate() + 1);
                var dateObject = new Date(dateValue);
                //Day
                var day = dateObject.getDate() + ".";
                day = (day.length == 2) ? ("0" + day) : day;
                //Month
                var month = (dateObject.getMonth()+1) + "."; 
                month = (month.length == 2) ? ("0" + month) : month;
                //Year
                var dateText =  day + month + dateObject.getFullYear();
                dates.push( dateText );
            }
            return dates;
	};

	this.convertDateToYYYYMMDD = function(dateText) {
		var pattern = /(\d{2})\.(\d{2})\.(\d{4})/;
		return dateText.replace(pattern,'$3-$2-$1');
	};
})

.service('DataInputService', function(){
	var priceInformationList;
	var activePriceInformationEntity;

	this.reset = function () {
		this.priceInformationList = [];
		this.activePriceInformationEntity = {};
	};

	this.getPriceInformationList = function() {
		return this.priceInformationList;
	};

	this.setProduct = function(product) {
		this.activePriceInformationEntity.product = product;
	};

	this.setPrice = function(price) {
		this.activePriceInformationEntity.price = price;
		this.priceInformationList.push(this.activePriceInformationEntity);
		this.activePriceInformationEntity = {};
	};

	this.isActivePriceEmpty = function() {
		return this.activePriceInformationEntity == {};
	};

	this.remove = function(priceInformation) {
		var index = -1;
		for(var i = 0, len = this.priceInformationList.length; i < len; i++) {
		    if (this.priceInformationList[i] === priceInformation) {
		        index = i;
		        break;
		    }
		}
		if(index != -1)
			this.priceInformationList.splice(index, 1);
	};
})

.service('AdMobService', function() {

	this.initAd = function() {
        if ( window.plugins && window.plugins.AdMob ) {
          var ad_units = {
        ios : {
          banner: 'ca-app-pub-4344154765553377/8516110246',
          interstitial: 'ca-app-pub-4344154765553377/8516110246'
        },
        android : {
          banner: 'ca-app-pub-4344154765553377/8516110246',
          interstitial: 'ca-app-pub-4344154765553377/8516110246'
        },
        wp8 : {
          banner: 'ca-app-pub-4344154765553377/8516110246',
          interstitial: 'ca-app-pub-4344154765553377/8516110246'
        }
          };
          var admobid = "";
          if( /(android)/i.test(navigator.userAgent) ) {
            admobid = ad_units.android;
          } else if(/(iphone|ipad)/i.test(navigator.userAgent)) {
            admobid = ad_units.ios;
          } else {
            admobid = ad_units.wp8;
          }
            window.plugins.AdMob.setOptions( {
                publisherId: admobid.banner,
                interstitialAdId: admobid.interstitial,
                bannerAtTop: false, // set to true, to put banner at top
                overlap: false, // set to true, to allow banner overlap webview
                offsetTopBar: false, // set to true to avoid ios7 status bar overlap
                isTesting: false, // receiving test ad
                autoShow: true // auto show interstitial ad when loaded
            });
            this.registerAdEvents();
        } else {
            console.log( 'admob plugin not ready' );
        }
    };
  
	this.registerAdEvents = function() {
      	document.addEventListener('onReceiveAd', function(){});
        document.addEventListener('onFailedToReceiveAd', function(data){});
        document.addEventListener('onPresentAd', function(){});
        document.addEventListener('onDismissAd', function(){ });
        document.addEventListener('onLeaveToAd', function(){ });
        document.addEventListener('onReceiveInterstitialAd', function(){ });
        document.addEventListener('onPresentInterstitialAd', function(){ });
        document.addEventListener('onDismissInterstitialAd', function(){ });
    };

    this.showAd = function() {
    	if(window.plugins && window.plugins.AdMob) {
	      this.initAd();
	      window.plugins.AdMob.createBannerView();
	    }
    };
})

.service('Credentials', function () {
  this.credential = {};

  this.get = function() {
    if(this.credential.username == null) {
        try {
          var data = window.localStorage.getItem('savedCredential');
          this.credential = JSON.parse(data);
          if(!this.credential)
            this.credential = {};
        } catch(err) {
          alert("Can not get credential! Error:" + err);
        }
    }
    return this.credential;
  };

  this.set = function(credential) {
      if(credential) {
        try {
          window.localStorage.setItem('savedCredential', JSON.stringify(credential));
          this.credential = credential;
        } catch (err) {
          alert("Can not set credential! Error:" + err);
        }
      }
  };
})

.service('Counter', function() {
  this.counter = 0;

  this.reset = function() {
    this.counter = 0;
  };

  this.increase = function() {
    this.counter++;
  };

  this.decrease = function() {
    this.counter--;
  };

  this.read = function() {
    return this.counter;
  };

});

;