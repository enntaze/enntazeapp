angular.module('starter.directives', [])

//https://gist.github.com/colllin/1a0c3a91cc641d8e578f#file-affixwithincontainer-js
.directive('sticky', ['$document', '$ionicScrollDelegate',
  function($document, $ionicScrollDelegate) {
 
  var transition = function(element, dy, executeImmediately) {
    /*
    element.style[ionic.CSS.TRANSFORM] == 'translate3d(0, -' + dy + 'px, 0)' ||
    executeImmediately ?
    element.style[ionic.CSS.TRANSFORM] = 'translate3d(0, -' + dy + 'px, 0)' :
    ionic.requestAnimationFrame(function() {
      element.style[ionic.CSS.TRANSFORM] = 'translate3d(0, -' + dy + 'px, 0)';
    });
    */
  };
 
  return {
    restrict: 'A',
    require: '^$ionicScroll',
    link: function($scope, $element, $attr, $ionicScroll) {
      var $affixContainer = $element.parent();
 
      var top = 0;
      var height = 0;
      var scrollMin = 0;
      var scrollMax = 0;
      var scrollTransition = 0;
      var affixedHeight = 0;
      var updateScrollLimits = _.throttle(function(scrollTop) {
          top = $affixContainer.offset().top;
          height = $affixContainer.outerHeight(false);
          affixedHeight = $element.outerHeight(false);
          scrollMin = scrollTop + top;
          scrollMax = scrollMin + height;
          scrollTransition = scrollMax - affixedHeight;
      }, 500, {
          trailing: false
      });
 
      var affix = null;
      var unaffix = null;
      var $affixedClone = null;
      var setupAffix = function() {
          unaffix = null;
          affix = function() {
              var top = 45;
              if(/(iphone|ipad)/i.test(navigator.userAgent)) {
                top = 0;
              }
              $affixedClone = $element.clone().css({
                  position: 'fixed',
                  top: top,
                  left: 0,
                  right: 0,
              });
              $affixedClone.addClass('sticky-header');
              //Prevent Clicking through
              $affixedClone.on('click', function(event) {
                event.stopPropagation();
              });

              $($ionicScroll.element).append($affixedClone);
 
              setupUnaffix();
          };
      };
      var cleanupAffix = function() {
          var r = $affixedClone && $affixedClone.remove();
          $affixedClone = null;
      };
      var setupUnaffix = function() {
          affix = null;
          unaffix = function() {
              cleanupAffix();
              setupAffix();
          };
      };
      $scope.$on('$destroy', cleanupAffix);
      setupAffix();
 
      var affixedJustNow;
      var scrollTop;
      $($ionicScroll.element).on('scroll', function(event) {
        scrollTop = (event.detail || event.originalEvent && event.originalEvent.detail).scrollTop;
        updateScrollLimits(scrollTop);
        scrollTop += 45;

        if (scrollTop >= scrollMin && scrollTop <= scrollMax) {
            affixedJustNow = affix ? affix() || true : false;
            if (scrollTop > scrollTransition) {
                transition($affixedClone[0], Math.floor(scrollTop-scrollTransition), affixedJustNow);
            } else {
                transition($affixedClone[0], 0, affixedJustNow);
            }
        } else {
           var r = unaffix && unaffix();
        }
      });
    }
  };
}])

.directive('gravatar', function () {
  
  var defaultGravatarUrl = "img/fide.png";
  var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  
  function getGravatarUrl(email) {      
    /*
    if (!regex.test(email))
      return defaultGravatarUrl;
    */    
    return 'http://www.gravatar.com/avatar/' + md5(email) + ".jpg?s=200&d=mm";
  }
  
  function linker(scope) {
    scope.url = getGravatarUrl(scope.email);
    
    scope.$watch('email', function (newVal, oldVal) {
      if (newVal !== oldVal) {
        scope.url = getGravatarUrl(scope.email);
      }
    });
  }
  
  return {
    template: '<img ng-src="{{url}}" fallback-src  ></img>',
    restrict: 'EA',
    replace: true,
    scope: { email: '=' }, 
    link: linker
  };  
})

.directive('intlTel', function(){
  return{
    replace:true,
    restrict: 'E',
    require: 'ngModel',
    template: '<input type="tel" >',
    link: function(scope,element,attrs,ngModel){
      var read = function() {
        var inputValue = element.val();
        ngModel.$setViewValue(inputValue);
      };      
      element.intlTelInput({
        defaultCountry:'tr',
      });
      element.on('focus blur keyup change', function() {
          scope.$apply(read);
      });

      element.on('keydown', function(event){
        // Once the limit has been met or exceeded, prevent all keypresses from working
        if (element.val().length >= 17){
          // Except backspace
          if (event.keyCode != 8){
            event.preventDefault();
          }
        } else if (element.val().length < 4) {
          event.preventDefault();
        }        
      });


      read();
    }
  };
})

.directive('focusOn', ['$timeout', '$parse',
  function($timeout, $parse) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
          scope.$watch(attrs.focusOn, function(newValue, oldValue) {
              if (newValue) { 
                element[0].focus();
              }
          });

          element.bind("blur", function(e) {
              $timeout(function() {
                  scope.$apply(attrs.focusOn + "=false"); 
              }, 0);
          });

          element.bind("focus", function(e) {
              $timeout(function() {
                  scope.$apply(attrs.focusOn + "=true");
              }, 0);
          });
      }
    };
}])

.directive('imgCache', ['$document', function ($document) {
    return {
      // require: 'ngSrc',
      link: function (scope, ele, attrs) {
        var target = $(ele);
        //waits for the event to be triggered,
        //before executing d call back
        scope.$on('ImgCacheReady', function () {
          //this checks if we have a cached copy.
          ImgCache.isCached(attrs.src, function(path, success){
            if(success){
              // already cached
              ImgCache.useCachedFile(target);
            } else {
              // not there, need to cache the image
              ImgCache.cacheFile(attrs.src, function(){
                ImgCache.useCachedFile(target);
              });
            }
          });
        }, false);
      }
    };
}])

;