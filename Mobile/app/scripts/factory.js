

var servicePath = 'http://localhost:8888/InstantMarket/git/Server/Trunk';
//var servicePath = 'http://localhost:8888/InstantMarket/Server/Trunk';
if( !window || !window.location || ! window.location.hostname || window.location.hostname != 'localhost' ) {
  servicePath = 'http://www.enntaze.com';
}

var restServicePath = servicePath + '/rest';
var crudServicePath = restServicePath + '/crud';
angular.module('starter.factories', [])

.factory('Product', ['$resource', function($resource) {
	var path = crudServicePath + '/product/:id';
	return $resource(path);
}])

.factory('TradeCenter', ['$resource', function($resource) {
	return $resource(crudServicePath + '/tradecenter/:id');
}])

.factory('PriceInformation', ['$resource', function($resource) {
    return $resource(crudServicePath + '/priceinformation/:id');
}])

.factory('Register', ['$resource', function($resource) {
    return $resource(restServicePath + '/register');
}])

.factory('LoginUser', ['$resource', function($resource) {
    return $resource(crudServicePath + '/loginuser/:id');
}])

.factory('PriceInformationlist', ['$resource', function($resource) {
    return $resource(restServicePath + '/priceinformationlist/:date/:tradecenterId/:productId');
}])

.factory('PriceInformationFeed', ['$resource', function($resource) {
    return $resource(restServicePath + '/priceinformationfeed/:count/:offset');
}])

.factory('ProfileInfo', ['$resource', function($resource) {
    return $resource(restServicePath + '/profile');
}])

.factory('ConnectionTestFactory', ['$resource', function($resource) {
    return $resource(restServicePath + '/test');
}])

.factory('Loading', ['$rootScope', '$ionicLoading', function($rootScope, $ionicLoading) {

  // Trigger the loading indicator
  return {
        show : function() { //code from the ionic framework doc
            
            // Show the loading overlay and text
            $rootScope.loading = $ionicLoading.show({

              // The text to display in the loading indicator
              content: 'Lütfen Bekleyiniz...',

              // The animation to use
              animation: 'fade-in',

              // Will a dark overlay or backdrop cover the entire view
              showBackdrop: false,

              // The maximum width of the loading indicator
              // Text will be wrapped if longer than maxWidth
              maxWidth: 200,

              // The delay in showing the indicator
              showDelay: 1000
            });
        },
        hide : function(){
          $ionicLoading.hide();
        }
    };
}])

.factory('MyKeyboard', ['$timeout' , function( $timeout) {
  return {
    show : function(id, delay) {      
      if(!delay || delay <= 0) delay = 10;
      $timeout(function() {
        try {          
          $("#" + id).focus();
          $("#" + id).select();
          var showKeyboard = cordova && cordova.plugins && cordova.plugins.Keyboard && cordova.plugins.Keyboard.show();
        } catch(err) { console.log('Keyboard could not shown'); }
      }, delay);
    },
    hide : function(delay) {      
      if(!delay || delay <= 0) delay = 0;
      $timeout(function() {
        try {
          var hideKeyboard = cordova && cordova.plugins && cordova.plugins.Keyboard && cordova.plugins.Keyboard.close();
        } catch(err) { console.log('Keyboard could not shown'); }
      }, delay);
    }
  };
}])

.factory('ConnectionFactory', [ '$log', function ($log) {	
	return function () {
	  var isConnected = false;
	  var networkConnection = navigator.connection;
	  if (!networkConnection.type) {
	    $log.error('networkConnection.type is not defined');
	    return false;
	  }
	  switch (networkConnection.type.toLowerCase()) {
	    case 'ethernet':
	    case 'wifi':
	    case 'cell_2g':
	    case 'cell_3g':
	    case 'cell_4g':
	    case '2g':
	    case '3g':
	    case '4g':
	    case 'cell':
	    case 'cellular':
	      isConnected = true;
	      break;
	  }
	  $log.log('isOnline? '+ isConnected);
	  return isConnected;
	};
}])

;