/* repeatString() returns a string which has been repeated a set number of times */ 
function repeatString(str, num) {
    out = '';
    for (var i = 0; i < num; i++) {
        out += str; 
    }
    return out;
}

/*
dump() displays the contents of a variable like var_dump() does in PHP. dump() is
better than typeof, because it can distinguish between array, null and object.  
Parameters:
  v:              The variable
  howDisplay:     "none", "body", "alert" (default)
  recursionLevel: Number of times the function has recursed when entering nested
                  objects or arrays. Each level of recursion adds extra space to the 
                  output to indicate level. Set to 0 by default.
Return Value:
  A string of the variable's contents 
Limitations:
  Can't pass an undefined variable to dump(). 
  dump() can't distinguish between int and float.
  dump() can't tell the original variable type of a member variable of an object.
  These limitations can't be fixed because these are *features* of JS. However, dump()
*/
function dump(v, howDisplay, recursionLevel) {
    howDisplay = (typeof howDisplay === 'undefined') ? "alert" : howDisplay;
    recursionLevel = (typeof recursionLevel !== 'number') ? 0 : recursionLevel;


    var vType = typeof v;
    var out = vType;

    switch (vType) {
        case "number":
            /* there is absolutely no way in JS to distinguish 2 from 2.0
            so 'number' is the best that you can do. The following doesn't work:
            var er = /^[0-9]+$/;
            if (!isNaN(v) && v % 1 === 0 && er.test(3.0))
                out = 'int';*/
        case "boolean":
            out += ": " + v;
            break;
        case "string":
            out += "(" + v.length + '): "' + v + '"';
            break;
        case "object":
            //check if null
            if (v === null) {
                out = "null";

            }
            //If using jQuery: if ($.isArray(v))
            //If using IE: if (isArray(v))
            //this should work for all browsers according to the ECMAScript standard:
            else if (Object.prototype.toString.call(v) === '[object Array]') {  
                out = 'array(' + v.length + '): {\n';
                for (var i = 0; i < v.length; i++) {
                    out += repeatString('   ', recursionLevel) + "   [" + i + "]:  " + 
                        dump(v[i], "none", recursionLevel + 1) + "\n";
                }
                out += repeatString('   ', recursionLevel) + "}";
            }
            else { //if object    
                sContents = "{\n";
                cnt = 0;
                for (var member in v) {
                    //No way to know the original data type of member, since JS
                    //always converts it to a string and no other way to parse objects.
                    sContents += repeatString('   ', recursionLevel) + "   " + member +
                        ":  " + dump(v[member], "none", recursionLevel + 1) + "\n";
                    cnt++;
                }
                sContents += repeatString('   ', recursionLevel) + "}";
                out += "(" + cnt + "): " + sContents;
            }
            break;
    }

    if (howDisplay == 'body') {
        var pre = document.createElement('pre');
        pre.innerHTML = out;
        document.body.appendChild(pre)
    }
    else if (howDisplay == 'alert') {
        alert(out);
    }

    return out;
}



// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.factories', 'starter.services', 'starter.directives' , 
  'starter.filters','ngResource' , 'angulartics', 'angulartics.google.tagmanager', 'angulartics.google.tagmanager.cordova',
  'ngCordova', 'ngPatternRestrict' , 'dcbImgFallback' ])

.run([ '$ionicPlatform', '$http', '$rootScope', 'Loading', '$ionicSideMenuDelegate', '$ionicHistory',
 function($ionicPlatform, $http, $rootScope, Loading, $ionicSideMenuDelegate, $ionicHistory) {

  $rootScope.$on('Loading:show', function() { Loading.show(); });
  $rootScope.$on('Loading:hide', function() { Loading.hide(); });

  function initImageCache() {
    //set up and init image caching
    // write log to console
    ImgCache.options.debug = true;
    // increase allocated space on Chrome to 50MB, default was 10MB
    ImgCache.options.chromeQuota = 50*1024*1024;
    ImgCache.init(function(){
      //small hack to dispatch an event when imgCache is 
      //full initialized.
      $rootScope.$broadcast('ImgCacheReady');
    }, function(){
        alert('ImgCache init: error! Check the log for errors');
    });
  }

  $ionicPlatform.ready(function($ionicPlatform) {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    initImageCache();

    $(".main-view").show(250);

  });

  $ionicPlatform.registerBackButtonAction(function () {
      if($ionicHistory.viewHistory().backView == null) {
        $ionicSideMenuDelegate.toggleLeft(true);
      } else {
        $ionicHistory.goBack(true);
      } 
    }, 100);

}])

.config(['$stateProvider', '$urlRouterProvider', '$resourceProvider', '$httpProvider', '$ionicConfigProvider', 'googleTagManagerCordovaProvider',
  function($stateProvider, $urlRouterProvider, $resourceProvider, $httpProvider , $ionicConfigProvider, googleTagManagerCordovaProvider ) {
  // Don't strip trailing slashes from calculated URLs
  $resourceProvider.defaults.stripTrailingSlashes = true;
  
  //Init Google Tag Manager Cordova
  googleTagManagerCordovaProvider.trackingId = 'GTM-5KS2JK';
  googleTagManagerCordovaProvider.period = 10; // default: 10 (in seconds)
  googleTagManagerCordovaProvider.debug = false; // default: false

  // note that you can also chain configs
  //$ionicConfigProvider.views.maxCache(1);
  $ionicConfigProvider.views.forwardCache(false);
  $ionicConfigProvider.backButton.text('');

  //Interceptor Setting
  $httpProvider.interceptors.push(function($rootScope, Credentials, Counter, $cordovaDialogs) {
    return {
      request: function(config) {             
		    var credentials = Credentials.get();
        $httpProvider.defaults.headers.common.username = credentials.username;
        $httpProvider.defaults.headers.common.password = credentials.password;        
        config.timeout = 10000;
        Counter.increase();
        $rootScope.$broadcast('Loading:show');
        return config;
      },
      requestError: function (rejection) {        
        $rootScope.$broadcast('scroll.refreshComplete');
        $rootScope.$broadcast('scroll.infiniteScrollComplete');
        $rootScope.$broadcast('Loading:hide');
        Counter.reset();
        $cordovaDialogs.alert("Bilgiler okunamadı. Lütfen tekrar deneyiniz", "Bağlantı hatası", "Tamam");
        return rejection;
      },
      response: function(response) {
        Counter.decrease();        
        if(Counter.read() == 0)
          $rootScope.$broadcast('Loading:hide');
        return response;
      },
      // On response failture
      responseError: function (rejection) {
        $rootScope.$broadcast('scroll.refreshComplete');
        $rootScope.$broadcast('scroll.infiniteScrollComplete');
        $rootScope.$broadcast('Loading:hide');
        Counter.reset();
        $cordovaDialogs.alert("Bilgiler okunamadı. Lütfen tekrar deneyiniz", "Bağlantı hatası", "Tamam");        
        return rejection;
      }
    };
  });

  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

  .state('app.register', {
    url:"/register",
    views: {
      'menuContent': {
        templateUrl: "templates/register.html",
        controller: 'RegisterController'
      }
    }
  })

  .state('app.priceinformationlistquery', {
    url:"/priceinformation/query",
    views: {
      'menuContent': {
        templateUrl : "templates/feed/feed.html",
        controller: 'FeedController'
      }
    }
  })

  .state('app.marketquery', {
    url:"/market/query",
    views: {
      'menuContent': {
        templateUrl: "templates/market/query.html",
        controller: 'QueryController'
      }
    }
  })

  .state('app.marketResultList', {
    url:"/market/resultList",
    views: {
      'menuContent': {
        templateUrl: "templates/market/resultList.html",
        controller: 'ResultListController'
      }
    }
  })

  .state('app.marketResultListProduct', {
    url: "/market/resultList/:productInformationId",
    views: {
      'menuContent': {
        templateUrl: "templates/market/resultOneProduct.html",
        controller: 'ResultOneProductController'
      }
    }
  })

  .state('app.dataInputDateAndTradeCenter', {
    url:"/dataInput/dateAndTradeCenter",
    views: {
      'menuContent': {
        templateUrl: "templates/dataInput/inputDateAndTradeCenter.html",
        controller: 'InputDateAndTradeCenterController'
      }
    }
  })

  .state('app.dataInputPriceInformation', {
    url:"/dataInput/priceInformation",
    views: {
      'menuContent': {
        templateUrl: "templates/dataInput/inputPriceInformation.html",
        controller: 'InputPriceInformationController'
      }
    }
  })

  .state('app.dataInputPriceInformationDetail', {
    url:"/dataInput/priceInformationDetail",
    views: {
      'menuContent': {
        templateUrl: "templates/dataInput/inputPriceInformationDetail.html",
        controller: 'InputPriceInformationDetailController'
      }
    }
  })

  .state('app.productInfo', {
    url:"/product/productList",
    views: {
      'menuContent': {
        templateUrl: "templates/product/productList.html",
        controller: 'ProductListController'
      }
    }
  })

  .state('app.addNewProduct', {
    url:"/product/addNewProduct",
    views: {
      'menuContent': {
        templateUrl: "templates/product/addNewProduct.html",
        controller: 'AddNewProductController'
      }
    }
  })

  .state('app.tradeCenterInfo', {
    url:"/tradeCenter/tradeCenterList",
    views: {
      'menuContent': {
        templateUrl: "templates/tradeCenter/tradeCenterList.html",
        controller: 'TradeCenterListController'
      }
    }
  })

  .state('app.addNewTradeCenter', {
    url:"/tradeCenter/addNewTradeCenter",
    views: {
      'menuContent': {
        templateUrl: "templates/tradeCenter/addNewTradeCenter.html",
        controller: 'AddNewTradeCenterController'
      }
    }
  })

  .state('app.profile', {
    url: "/profile",
    views: {
      'menuContent': {
        templateUrl: "templates/profile/profile.html",
        controller: 'ProfileController'
      }
    }
  })

  .state('app.about', {
    url: "/about",
    views: {
      'menuContent': {
        templateUrl: "templates/about/about.html"
      }
    }
  })

  .state('app.webinfo', {
    url: "/webinfo/:title",
    views: {
      'menuContent': {
        templateUrl: "templates/webinfo/webinfo.html",
        controller: 'WebInfoController'
      }
    }
  })
  ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/register');

}]);
