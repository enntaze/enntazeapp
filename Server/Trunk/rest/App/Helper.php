<?php

function randomUsername() {
	$username = time();
 	$username .= "_";
 	$username .= randomPassword();
	return $username;
}

function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function priceinformationlist($date, $tradecenterId, $productId) {
    $sql = "select  *
            from (
            select
            CONCAT(trade_center_id, product_id, DATE_FORMAT(date, '%Y%m%d')) as 'id',
            trade_center_id, 
            tradecenter.name as 'tradeCenterName', 
            product_id, 
            product.name as 'productName',
            product.image as 'img',
            DATE_FORMAT(date, '%Y-%m-%d') as 'date',
            price,
            count(points) as 'count',
            sum(points) as 'points'
            from priceinformation
            left join product on product.id = priceinformation.product_id
            left join tradecenter on tradecenter.id = priceinformation.trade_center_id
            left join loginuser on loginuser.username = priceinformation.update_user
            where priceinformation.is_deleted = 0
            group by trade_center_id, product_id, date, price
            ) as firstQuery
            where 
                date = :date 
                and trade_center_id = IF( :tradeCenterId != 0 , :tradeCenterId , trade_center_id)
                and product_id = IF( :productId != 0 , :productId , product_id)
            order by tradeCenterName, productName, points desc";

    $queryDate = date_create($date);    
    $rows = R::getAll( $sql , [ ':date' => date_format($queryDate,"Y-m-d") , ':tradeCenterId' => $tradecenterId , ':productId' => $productId ] );
    $json = json_encode($rows);
    echo $json;
}

function priceinformationfeed($count, $offset) {
    $sql = "select 
            IF(loginuser.namesurname IS NULL OR loginuser.namesurname = '', 'Mehmet Demir', loginuser.namesurname) as 'nameSurname',
            loginuser.email as 'email',
            loginuser.telephone as 'telephone',
            tradecenter.name as 'tradeCenterName', 
            tradecenter.id as 'tradeCenterId',
            product.name as 'productName',
            product.id as 'productId',
            product.image as 'imageUrl',
            priceinformation.price as 'price',
            priceinformation.date as 'date',
            priceinformation.create_date as 'feedDate'

            from priceinformation 
            left join tradecenter on tradecenter.id = priceinformation.trade_center_id
            left join product on product.id = priceinformation.product_id
            left join loginuser on loginuser.username = priceinformation.update_user
            where priceinformation.is_deleted = 0
            order by priceinformation.create_date desc
            LIMIT :limit 
            OFFSET :offset";

    if(intval($count) > 100)
        $count = 100;

    $rows = R::getAll($sql, [ ':limit' => intval($count) , ':offset' => intval($offset) ] );
    $json = json_encode($rows);
    echo $json;
}

function findEntity($entities, $searchField, $searchText) {
    $log = false; //Enable Log (false | true);
    $foundEntity = null;
    $searchText = trim($searchText);
    if($log) print "<br><br>" . " Searching $searchText in array" . "<br>";
    foreach ($entities as $entity) {
        if($log) print "Value is : $entity[$searchField] ";        
        
        //if( trim($entity[$searchField]) == $searchText) 
        if( strpos($entity[$searchField], $searchText) !== false)        
        {
            if($log) print "Match Succeed!" . "<br>";
            $foundEntity = $entity;
            break;
        }
        if($log) print "No match!" . "<br>";
    }
    return $foundEntity;
}

function syncWithMersin() {
    $html = file_get_html('http://www.mersin.bel.tr/kose/kategori-belediye-hal.asp?sira=1');
    // Find all images 
    foreach($html->find('tr') as $row) {
        if(count($row->children()) == 3) 
        {
            echo $row->childNodes(0)->plaintext . ', ';
            echo $row->childNodes(1)->plaintext . ' - ';
            echo $row->childNodes(2)->plaintext . '<br> ';

        }
    }
}

function syncWithTarim( $startIndex , $endIndex ) { 

    $startIndex = (int)$startIndex;
    $endIndex = (int)$endIndex;

    setlocale(LC_ALL, 'tr_TR.UTF-8');
    echo "Syncing Now : " . strftime("%e %B %Y %A %H:%M:%S", time()) . "<br>";

    $productSql = "select id as 'id' , tarimname as 'name' from product where is_deleted != :isDeleted";
    $allProducts = R::getAll($productSql, [ ':isDeleted' => intval(1) ] );

    $tradeCenterSql = "select id as 'id' , tarimname as 'name' from tradecenter where is_deleted != :isDeleted";
    $allTradeCenters = R::getAll($tradeCenterSql, [ ':isDeleted' => intval(1) ] );    

    $turMonths = array("Oca", "Sub", "Mar", "Nis", "May", "Haz", "Tem", "Agu", "Eyl", "Eki", "Kas", "Ara");
    $engMonths   = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Agu", "Sep", "Oct", "Nov", "Dec");


    for($addressIndex = $startIndex ; $addressIndex <= $endIndex ; $addressIndex++) {
        $address = "http://tarim.com.tr/Hal-Fiyatlari.aspx?Sayfa=" . $addressIndex;
        echo "<br>" . "address : " . $address;
        $html = file_get_html($address);
        $childCount = 6;
        foreach($html->find('tr') as $row) {
            if(count($row->children()) == $childCount) 
            {
                $product = findEntity($allProducts, "name", $row->childNodes(0)->plaintext );
                $tradecenter = findEntity($allTradeCenters, "name", $row->childNodes(2)->plaintext );
                if($product && $tradecenter) {

                    print "<br>" . "--Sync : ";
                    for ($i = 0; $i < $childCount; $i++) {
                        echo $row->childNodes($i)->plaintext . ', ';
                    }

                    $dateText = trim($row->childNodes(5)->plaintext);
                    $dateText = str_replace($turMonths, $engMonths, $dateText);

                    echo "Datetext " . $dateText . " | ";
                    $dateParts = explode(",", $dateText);                    
                    echo "strtotime : " . strtotime($dateParts[0]);

                    $date = date('Y-m-d', strtotime($dateParts[0]));
                    $price = str_replace(',','.', trim($row->childNodes(4)->plaintext));

                    $entity = R::dispense( 'priceinformation' );
                    $entity->product_id = $product["id"];
                    $entity->trade_center_id = $tradecenter["id"];
                    $entity->date = $date;
                    $entity->price = $price;
                    
                    $id = R::store( $entity );
                    print "\t" . "Stored with Id : $id";
                } else {
                    // Row is not valid to sync
                }
            }
        }
    }
}



?>