<?php

	class Model_Base extends RedBean_SimpleModel {
        public function open() {
        }
        public function dispense(){
            $this->isDeleted = false;
        }
        public function update() {
            global $authUser;
        	$now = date("Y-m-d H:i:s");
        	$this->updateDate = $now;
            $this->updateUser = $authUser->username;
	        if($this->id == 0)
	        {
	            $this->createDate = $now;
                $this->createUser = $authUser->username;
	        }
        }
        public function after_update(){
        }
        public function delete() {
        }
        public function after_delete() {
        }
    }

    class Model_ServiceLog extends Model_Base {}
    class Model_Product extends Model_Base {}
    class Model_TradeCenter extends Model_Base {}
    class Model_PriceInformation extends Model_Base {
        public function update(){            
            parent::update();
            R::exec( 'UPDATE priceinformation set is_deleted=1 WHERE product_id = :productId and trade_center_id = :tradeCenterId and update_user = :username and date = :date ', 
                [':productId' => $this->productId , ':tradeCenterId' => $this->tradeCenterId, ':username' => $this->update_user, ':date' => $this->date ] );
        }
    }    
    class Model_User extends Model_Base {}
    class Model_LoginUser extends Model_Base {
        public function dispense() {
            parent::dispense();
            $this->points = 10;
            $this->username = randomUsername();
            $this->password = randomPassword();
            $this->namesurname = "";
            $this->email = "";
            $this->telephone = "";
        }
    }

?>