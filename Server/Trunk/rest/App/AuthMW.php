
<?php

global $authUser;

class AuthMW extends \Slim\Middleware
{
    public function call()
    {
      // Get reference to application
       $app = $this->app;

       $isRegister = ($app->request->getPathInfo() == '/register');
       $isTest = ($app->request->getPathInfo() == '/test');
       $isSync = strpos( $app->request->getPathInfo() , "/sync/") !== false;       
       $isOptions = $app->request->isOptions();
       $isSpecialCase = $isOptions || $isRegister || $isTest || $isSync;

       $username = $app->request->headers->get('username');
       $password = $app->request->headers->get('password');
       global $authUser;
       $authUser = R::findOne( 'loginuser', ' username = ? and password = ? and is_deleted = 0 ', [ $username, $password ]);

       if($isSpecialCase)
       {
          $authUser = json_decode("{\"username\":\"system\", \"id\":-1}");
          $sysUser = R::findOne( 'loginuser', ' username = system and is_deleted = 0 ');
          if(!$sysUser) {
            $loginUser = R::dispense( 'loginuser' );
            $loginUser->username = "system";
            $id = R::store( $loginUser );
          }
       }
       if(!$authUser || $authUser->id == 0 )
       {
          $app->halt(401, 'Authentication required!');
       } 
       $this->next->call();
    }
}

?>