<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */

require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

require 'App/Helper.php';
require 'App/AuthMW.php';
require '../redbean/rb.php';
include 'App/EntityModel.php';
require '../htmlparser/simple_html_dom.php';

//R::setup();
R::setup('mysql:host=localhost;port=8888;dbname=mydatabase', 'root','root'); //for both mysql or mariaDB
//R::setup('mysql:host=localhost;port=8888;dbname=piyasa', 'piyasa_user','Piyasa2015!'); //for both mysql or mariaDB

include 'App/ReInitDb.php'; //Control to Reset DB

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

/**
 * Set MiddleWare Ex: Authentication
 */
$app->add(new \AuthMW());


/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */

//  Request - Response Logger
$app->hook('slim.after', function () use ($app) {

    $serviceLog = R::dispense( 'servicelog' );    
    //Request Info
    $serviceLog->scheme             = $app->request->getScheme();
    $serviceLog->host               = $app->request->getHost();
    $serviceLog->port               = $app->request->getPort();
    $serviceLog->rootUri            = $app->request->getRootUri();
    $serviceLog->resourceUri        = $app->request->getResourceUri();
    $serviceLog->method             = $app->request->getMethod();
    $inputData = file_get_contents("php://input");
    $serviceLog->requestBody        = json_encode($inputData );

    //Response Info
    $body = $app->response->getBody();
    $serviceLog->responseBody        = json_encode($body );
    $serviceLog->hostIp             = $app->request->getIp();
    $serviceLog->referrer           = $app->request->getReferrer();
    $serviceLog->useragent          = json_encode( $app->request->getUserAgent() );

    $id = R::store( $serviceLog );
});

$app->map('/test', function() {
    echo "OK!";
})->via('GET', 'POST', 'PUT');

$app->get('/sync/:source/:startIndex/:endIndex', function($source, $startIndex, $endIndex) use ($app) {
    
    if($source == 'mersin') {
        syncWithMersin();
    } else if ($source == 'tarim') {
        syncWithTarim( $startIndex, $endIndex );    
    } else {
        echo "source $source could not fount...";
    }
});

$app->post('/register', function() use ($app) {
    $inputData = file_get_contents("php://input");
    $jsonPostData = json_decode($inputData);
    $loginUser = R::dispense( 'loginuser' );
    $loginUser->import($jsonPostData);
    $id = R::store( $loginUser );
    echo $loginUser;
});

// CRUD group
$app->group('/crud', function () use ($app) {
    //Retrive All
    $app->get('/:entityName', function($entityName) {
        $dataArray = R::getAll(  "select * from " . $entityName . " where is_deleted <> 1" );
        $count = count($dataArray);
        for($i = 0; $i< $count; $i++) {
            unset($dataArray[$i]['is_deleted']);
            unset($dataArray[$i]['update_date']);
            unset($dataArray[$i]['update_user']);
            unset($dataArray[$i]['create_date']);
            unset($dataArray[$i]['create_user']);
        }
        echo json_encode($dataArray);
    });
    //Retrieve By Id
    $app->get('/:entityName/:id', function($entityName, $id) {
        $entity = R::load( $entityName, $id );
        $array = $entity->export();
        echo json_encode($array);
    });
    //Create | Update
    $app->post('/:entityName', function($entityName) use ($app) {
        $inputData = file_get_contents("php://input");
        $jsonPostData = json_decode($inputData);
        $entity = R::dispense( $entityName );
        $entity->import($jsonPostData);
        $operation = ($entity->id == 0 ? "created" : "updated"); 
        $id = R::store( $entity );
        print "$entityName entity is $operation with success.";
    });
    //Delete Error
    $app->delete('/:entityName', function($entityName) use ($app) {
        print "ERR: id value of $entityName entity is not supplied! Please check.";
    });
    //Delete
    $app->delete('/:entityName/:id', function($entityName, $id) use ($app) {    
        $entity = R::load( $entityName, $id );
        if($entity->id == 0)
        {
            print "Could not find $entityName entity with Id : $id";
        }
        else
        {
            $entity->isDeleted = true;
            $id = R::store( $entity );
            print "Deleted $entityName entity with Id: $id";
        }    
    });
    //Handle Rest!
    $app->get('/', function() use ($app) {
        echo "Empty not implemented!!";
    });
    $app->get('/:params+', function($params) use ($app) {
        echo "Not implemented!! ";
        echo $params;
    });
});

$app->get('/profile', function() use ($app) {
    global $authUser;
    print "{ \"name\": \"$authUser->namesurname\" , \"email\":\"$authUser->email\" , \"telephone\":\"$authUser->telephone\" }";    
});

$app->post('/profile', function() use ($app) {
    $inputData = file_get_contents("php://input");
    $jsonPostData = json_decode($inputData);
    global $authUser;
    $authUser->namesurname = $jsonPostData->name;
    $authUser->email = $jsonPostData->email;
    $authUser->telephone = $jsonPostData->telephone;
    R::store($authUser);
    echo "{ \"message\":\"İşlem Başarılı\" }";
});

$app->get('/priceinformationlist/:date/:tradecenterId/:productId', function($date, $tradecenterId, $productId) use ($app) {
    priceinformationlist($date, $tradecenterId, $productId);  
});

$app->get('/priceinformationfeed/:count/:offset', function($count, $offset) use ($app) {
    priceinformationfeed($count, $offset);
});

$app->get('/:params+', function($params) use ($app) {
    echo "Route : ";
    echo json_encode($params);
    echo " is not implemented!";
});

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
